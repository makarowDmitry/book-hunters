package ru.makar.bookhunter.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class MainParser {
    Document connectionParser(String url) throws IOException {
        return Jsoup.connect(url)
                .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2919.83 Safari/537.36")
                .referrer("https://yandex.ru/")
                .get();
    }

    Elements getElements(Document doc, String cssQuery){
        return doc.select(cssQuery);
    }


    ArrayList<String> getDataOfSite(Elements elements, String query){
        ArrayList<String> arrayList = new ArrayList<String>();
        for (Element element:elements.select(query)
        ) {
            //System.out.println(element.text());
            arrayList.add(element.text());
        }
        return arrayList;
    }


    ArrayList<String> getAttr(Elements elements, String query, String attrTag){
        ArrayList<String> arrayList = new ArrayList<>();
        for (Element element:elements.select(query)) {
            arrayList.add(element.attr(attrTag));
        }
        return arrayList;
    }


}
