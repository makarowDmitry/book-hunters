package ru.makar.bookhunter.parser;

public interface UrlAndClass {

    String[] arrGenre = {"Драматургия"};

    //Ссылки на страницы
        //Лабиринт
        String urlDramaLabirint = "https://www.labirint.ru/genres/2556/?page="; //Страница каталога драм
        String urlDetectLabirint = "https://www.labirint.ru/genres/2498/?page="; //Страница каталога детективов


        String urlBookLabirint = "https://www.labirint.ru/books/"; //Страница книги
        //Буквоед
        String urlBookvoedGenres = "https://www.bookvoed.ru/books?genre=26"; //Страница откуда брать название жанров художественной литературы
        //Book24
        String urlBook24Genres = "https://book24.ru/catalog/fiction-1592/"; //Страница откуда брать название жанров художественной литературы
    //Классы
        //Лабиринт
        String classBlockBookLabirint = "div.catalog-responsive"; //Класс каталога книги

        String classNameBookLabirint = "a.product-title-link";//Класс названий книги
        String classAuthorBookLabirint = "div.product-author"; //Класс имен авторов
        String classPublisherBookLabirint = "a.product-pubhouse__pubhouse";//Класс издательств
        String classSeriesBookLabirint = "a.product-pubhouse__series"; //Класс серий
        String classImageBookLabirint = "img.book-img-cover"; //Класс картинки

        String classCodeBookLabirint = "div.product.need-watch"; //Класс хранящий в себе код старницы книги
        String attrBlock = "data-product-id";
        String attrImage = "data-src";

        String classBlockBookDescriptLabirint = "div.product-description"; // Класс краткой информации о книге на её странице
        String classPricesBookLabirint = "span.buying-pricenew-val-number"; //Класс цен
        String classBlockBookBigDescriptLabirint = "div.product-right-column";
        String classRateLabirint = "div#rite";//Класс блока с оценкой
        String classDescriptionLabirint = "div#product-about";//Класс с описанием книги

        //Буквоед
        String classBookvoedGenres = "ul.dsb"; //Класс списка жанров художественной литературы
        String classTextBookvoedGenres = "a.esb"; //Класс с текстовым названием жанров худ. лит.
        //Book24
        String classBook24Genres = "li.catalog-navigations__item"; //Класс списка жанров художественной литературы
        String classTextBook24Genres = "a.catalog-navigations__link"; //Класс с текстовым названием жанров худ. лит.

}
