package ru.makar.bookhunter.parser;

import lombok.Data;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

@Data
public class Book24Parser implements UrlAndClass {

    MainParser mainParser = new MainParser();

    Document docBook24;

    {
        try {
            docBook24 = mainParser.connectionParser(urlBook24Genres);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    Elements ElementsListGenresBook24 = mainParser.getElements(docBook24, classBook24Genres);

    ArrayList<String> listGenresBook24 = new ArrayList<>(); //Список жанров худ. лит. из магазина Лабиринт

    public void getGenresBook24(){
        for (Element element:ElementsListGenresBook24.select(classTextBook24Genres)
        ) {
            listGenresBook24.add(element.text());
        }
        listGenresBook24.remove(0);
        for (int i = 0; i <listGenresBook24.size() ; i++) {
            System.out.println(listGenresBook24.get(i));
        }
    }


}


