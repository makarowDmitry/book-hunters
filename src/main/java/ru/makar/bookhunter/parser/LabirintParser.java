package ru.makar.bookhunter.parser;


import ru.makar.bookhunter.models.Book;
import ru.makar.bookhunter.models.Price;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

public class LabirintParser implements UrlAndClass {
    MainParser mainParser = new MainParser();


    private String[] arrPages = {urlDramaLabirint, urlDetectLabirint};


    private ArrayList<String> bookName = new ArrayList<>();
    private ArrayList<String> bookAuthor = new ArrayList<>();
    private ArrayList<String> bookPublisher = new ArrayList<>();
    private ArrayList<String> bookSeries = new ArrayList<>();
    private ArrayList<String> bookImage = new ArrayList<>();
    private ArrayList<String> bookDescription = new ArrayList<>();
    private ArrayList<String> bookRite = new ArrayList<>();

    private ArrayList<String> bookPrices = new ArrayList<>();

    public void getDramaBookLabirint(int numPages) throws IOException {
        bookName = mainParser.getDataOfSite(mainParser.getElements(mainParser.connectionParser(urlDramaLabirint + 1), classBlockBookLabirint), classNameBookLabirint);
        bookAuthor = mainParser.getDataOfSite(mainParser.getElements(mainParser.connectionParser(urlDramaLabirint + 1), classBlockBookLabirint), classAuthorBookLabirint);
        bookPublisher = mainParser.getDataOfSite(mainParser.getElements(mainParser.connectionParser(urlDramaLabirint + 1), classBlockBookLabirint), classPublisherBookLabirint);
        bookSeries = mainParser.getDataOfSite(mainParser.getElements(mainParser.connectionParser(urlDramaLabirint + 1), classBlockBookLabirint), classSeriesBookLabirint);
        bookImage = mainParser.getAttr(mainParser.getElements(mainParser.connectionParser(urlDetectLabirint + 1), classBlockBookLabirint), classImageBookLabirint, attrImage);

        ArrayList<String> codeBook = mainParser.getAttr(mainParser.getElements(mainParser.connectionParser(urlDramaLabirint + 1), classBlockBookLabirint), classCodeBookLabirint, attrBlock);
        for (int i = 2; i <= numPages; i++) {
            bookName.addAll(mainParser.getDataOfSite(mainParser.getElements(mainParser.connectionParser(urlDramaLabirint + i), classBlockBookLabirint), classNameBookLabirint));
            bookAuthor.addAll(mainParser.getDataOfSite(mainParser.getElements(mainParser.connectionParser(urlDramaLabirint + i), classBlockBookLabirint), classAuthorBookLabirint));
            bookPublisher.addAll(mainParser.getDataOfSite(mainParser.getElements(mainParser.connectionParser(urlDramaLabirint + i), classBlockBookLabirint), classPublisherBookLabirint));
            bookSeries.addAll(mainParser.getDataOfSite(mainParser.getElements(mainParser.connectionParser(urlDramaLabirint + i), classBlockBookLabirint), classSeriesBookLabirint));
            bookImage.addAll(mainParser.getAttr(mainParser.getElements(mainParser.connectionParser(urlDramaLabirint + i), classBlockBookLabirint), classImageBookLabirint, attrImage));
            codeBook.addAll(mainParser.getAttr(mainParser.getElements(mainParser.connectionParser(urlDramaLabirint + i), classBlockBookLabirint), classCodeBookLabirint, attrBlock));
        }
        System.out.println("End One for");

        bookPrices = mainParser.getDataOfSite(mainParser.getElements(mainParser.connectionParser(urlBookLabirint + codeBook.get(0)), classBlockBookDescriptLabirint), classPricesBookLabirint);
        bookDescription = mainParser.getDataOfSite(mainParser.getElements(mainParser.connectionParser(urlBookLabirint + codeBook.get(0)), classDescriptionLabirint), "p");
        bookRite = mainParser.getDataOfSite(mainParser.getElements(mainParser.connectionParser(urlBookLabirint + codeBook.get(0)), classBlockBookBigDescriptLabirint), classRateLabirint);

        for (int y = 2; y <= codeBook.size() - 1; y++) {
            bookPrices.addAll(mainParser.getDataOfSite(mainParser.getElements(mainParser.connectionParser(urlBookLabirint + codeBook.get(y)), classBlockBookDescriptLabirint), classPricesBookLabirint));
            bookDescription.addAll(mainParser.getDataOfSite(mainParser.getElements(mainParser.connectionParser(urlBookLabirint + codeBook.get(y)), classDescriptionLabirint), "p"));
            bookRite.addAll(mainParser.getDataOfSite(mainParser.getElements(mainParser.connectionParser(urlBookLabirint + codeBook.get(y)), classBlockBookBigDescriptLabirint), classRateLabirint));
        }
        System.out.println("Book:" + bookName.size() + "Author:" + bookAuthor.size() + "publisher:" + bookPublisher.size() + "serias:" + bookSeries.size() + "image" + bookImage.size() + "description" + bookDescription.size() + "rite" + bookRite.size() + "price" + bookPrices.size());

        testArraylist();
    }

    private void testArraylist() {
        if (bookName.size() > bookSeries.size()) {
            for (int i = bookSeries.size() - 1; i < bookName.size() - 1; i++) {
                bookSeries.add("Информация отсутствует");
            }
        }
        if (bookName.size() > bookAuthor.size()) {
            for (int i = bookAuthor.size() - 1; i < bookName.size() - 1; i++) {
                bookAuthor.add("Информация отсутствует");
            }
        }
        if (bookName.size() > bookRite.size()) {
            for (int i = bookRite.size(); i < bookName.size() - 1; i++) {
                bookRite.add("-");
            }
        }
        if (bookName.size() > bookPrices.size()) {
            for (int i = bookPrices.size() - 1; i < bookName.size() - 1; i++) {
                bookPrices.add("Информация отсутсвует");
            }
        }

    }

    public ArrayList<Book> getBooks() {
        System.out.println("Start getBook");
        ArrayList<Book> listBooks = new ArrayList<>();
        ArrayList<Price> prices = getPrice();

        for (int i = 0; i < bookName.size() - 1; i++) {
            Book book = Book.builder()
                    .name(bookName.get(i))
                    .author(bookAuthor.get(i))
                    .publisher(bookPublisher.get(i))
                    .series(bookSeries.get(i))
                    .image(bookImage.get(i))
                    .description(bookDescription.get(i))
                    .assessment(bookRite.get(i))
                    .genre(arrGenre[0])
                    .build();


            /*prices.get(i).setBook(book);*/
            /*book.getPrices().add(prices.get(i));*/

            listBooks.add(book);
        }
        System.out.println("End getBook");
        return listBooks;
    }

    public ArrayList<Price> getPrice() {
        ArrayList<Price> listPrice = new ArrayList<>();
        for (int i = 0; i < bookName.size() - 1; i++) {
            Price price = Price.builder()
                    .shop("Лабиринт")
                    .price(bookPrices.get(i))
                    .build();
            listPrice.add(price);
        }
        return listPrice;
    }
}





