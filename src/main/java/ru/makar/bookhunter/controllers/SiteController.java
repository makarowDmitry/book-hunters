package ru.makar.bookhunter.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.makar.bookhunter.models.Book;
import ru.makar.bookhunter.parser.LabirintParser;
import ru.makar.bookhunter.repo.BookRepo;

import java.io.IOException;
import java.util.ArrayList;

@Controller
public class SiteController {

    @Autowired
    private BookRepo bookRepo;

    @GetMapping
    public String index(Model model) {
        model.addAttribute("title", "Главная страница");
        return "index";
    }

    @GetMapping("/update")
    public void updateBook(@RequestParam("page") Integer page) {
        try {
            if (page != 0) {

                LabirintParser labirintParser = new LabirintParser();
                labirintParser.getDramaBookLabirint(page);
                bookRepo.saveAll(labirintParser.getBooks());

                //priceRepo.saveAll(labirintParser.getPrice());

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
