package ru.makar.bookhunter.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.makar.bookhunter.models.Price;

public interface PriceRepo extends JpaRepository<Price, Long> {
}
