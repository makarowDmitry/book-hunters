package ru.makar.bookhunter.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.makar.bookhunter.models.Book;

public interface BookRepo extends JpaRepository<Book, Long> {

}
