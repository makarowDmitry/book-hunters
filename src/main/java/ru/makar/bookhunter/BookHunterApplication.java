package ru.makar.bookhunter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.makar.bookhunter.repo.BookRepo;
import ru.makar.bookhunter.repo.PriceRepo;

@SpringBootApplication
public class BookHunterApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookHunterApplication.class, args);
        /*LabirintParser labirintParser = new LabirintParser();
        try {
            labirintParser.getDramaBookLabirint(8);
            ArrayList<Book> listData = labirintParser.getBook();
            System.out.println(listData.size());
            System.out.println(listData.toString());

           // bookRepo.saveAll(labirintParser.getBook());

            //priceRepo.saveAll(labirintParser.getPrice());
        } catch (IOException e) {
            e.printStackTrace();
        }*/

    }

}
