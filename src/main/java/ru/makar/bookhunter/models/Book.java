package ru.makar.bookhunter.models;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Set;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "books_data")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, unique = true)
    Long id;

    @Column(nullable = false)
    String name;

    @Column()
    String author;

    @Column(nullable = false)
    String publisher;

    @Column()
    String series;

    @Column()
    String image;

    @Column()
    @Type(type = "text")
    String description;

    @Column()
    String assessment;

    /*@Column()
    Integer assessmentUser;*/

    @Column(nullable = false)
    String genre;

    /*@Column()
    String binding;*/


    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "favsBook")
    Set<User> users;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "book")
    Set<Price> prices;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "book")
    Set<DesiredPrice> desiredPrices;// Если не будет работать поменять на collection

}
