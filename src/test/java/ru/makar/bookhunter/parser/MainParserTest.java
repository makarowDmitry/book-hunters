package ru.makar.bookhunter.parser;

import org.jsoup.select.Elements;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class MainParserTest implements UrlAndClass {



    MainParser mainParser = new MainParser();

    @Test
    public void checkNumberGetDataOfSite(){
        Elements elements = null;

        try {
            elements = mainParser.getElements(mainParser.connectionParser(urlDramaLabirint), classBlockBookLabirint);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ArrayList<String> testArrayList = mainParser.getDataOfSite(elements, classNameBookLabirint);

        assertEquals(testArrayList.size(), 58);
    }

    @Test
    public void checkGetDataOfSite(){
        Elements elements = null;

        try {
            elements = mainParser.getElements(mainParser.connectionParser(urlBookLabirint + "799287"), classDescriptionLabirint);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ArrayList<String> testArrayList = mainParser.getDataOfSite(elements, "p");

        assertEquals(testArrayList.get(0), "Крупнейший английский драматург конца XIX - первой половины XX в. Джордж Бернард Шоу (1856-1950) в своих произведениях выступает как мастер интеллектуальной драмы-дискуссии, построенной на острых диалогах, полной парадоксальных ситуаций, разрушающей все традиционные представления о театре. Его пьесы бичуют политическую реакцию, нормативную мораль, лицемерие, ханжество. В 1925 г. писателю была присуждена Нобелевская премия. В данный сборник вошли пьесы \"Пигмалион\", \"Цезарь и Клеопатра\" и \"Дом, где разбиваются сердца\", ставшие классикой театральных сцен всего мира.");
    }


}